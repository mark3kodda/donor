package test.java.com.progect1.cache;

import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PersonRepoTest {

    private final PersonRepo cut = new PersonRepo();

    static Arguments[] addPersonShouldAddPersonToEmptyListArgs() {
        return new Arguments[]{
                Arguments.arguments(
                        new Person[]{},
                        new Person(1, "Maxim", "Biba", 34, "Kiev"),
                        new Person[]{new Person(1, "Maxim", "Biba", 34, "Kiev")}
                ),
                Arguments.arguments(
                        new Person[]{new Person(1, "Maxim", "Biba", 34, "Kiev")},
                        new Person(2, "Maxim2", "Biba2", 34, "Kiev2"),
                        new Person[]{
                                new Person(1, "Maxim", "Biba", 34, "Kiev"),
                                new Person(2, "Maxim2", "Biba2", 34, "Kiev2")
                        }
                ),
        };
    }

    @ParameterizedTest
    @MethodSource("addPersonShouldAddPersonToEmptyListArgs")
    void addPersonShouldAddPersonToEmptyList(Person[] repoStartArray, Person personToAdd, Person[] expected) {
        //Given
        cut.setPersons(repoStartArray);
        //When
        cut.add(personToAdd);
        //Then
        Person[] actual = cut.getPersons();

        Assertions.assertArrayEquals(expected, actual);

    }

    @Test
    void addPersonShouldAddPersonToNotEmptyList() {
        //Given
        Person person2 = new Person(2, "Maxim2", "Biba2", 34, "Kiev2");
        Person person = new Person(1, "Maxim", "Biba", 34, "Kiev");
        Person[] expected = {person2, person};
        cut.setPersons(new Person[]{person2});
        //When
        cut.add(person);
        //Then
        Person[] actual = cut.getPersons();

        Assertions.assertArrayEquals(expected, actual);

    }


    @Test
    void removePersonFromList() {
        //Given
        Person person2 = new Person(2, "Maxim2", "Biba2", 34, "Kiev2");
        Person person = new Person(1, "Maxim", "Biba", 34, "Kiev");
        Person[] expected = {person};
        cut.setPersons(new Person[]{person2, person});
        //When
        cut.remove(person2);
        //Then
        Person[] actual = cut.getPersons();

        Assertions.assertArrayEquals(expected, actual);

    }

    static Arguments[] getMaxIdFromListArgs() {
        return new Arguments[]{
                Arguments.arguments(new Person[]{new Person(2, "Maxim2", "Biba2", 34, "Kiev2")
                        , new Person(1, "Maxim", "Biba", 34, "Kiev")}, 2),
                Arguments.arguments(new Person[]{}, 0),

        };
    }

    @ParameterizedTest
    @MethodSource("getMaxIdFromListArgs")
    void getMaxIdFromList(Person[] persons, int expected) {
        // Given

        cut.setPersons(persons);
        // When
        int actual = cut.getMaxId();
        // Then
        Assertions.assertEquals(expected, actual);
    }



    static Arguments[] getPersonArgs() {
        return new Arguments[]{
                Arguments.arguments(new Person[]{new Person(2, "Maxim2", "Biba2", 34, "Kiev2")
                        , new Person(1, "Maxim", "Biba", 34, "Kiev")},
                        new Person(2, "Maxim2", "Biba2", 34, "Kiev2") ),
                Arguments.arguments(new Person[]{new Person(1, "Maxim2", "Biba2", 34, "Kiev2")
                                , new Person(3, "Maxim", "Biba", 34, "Kiev")}, null),
        };
    }

    @ParameterizedTest
    @MethodSource("getPersonArgs")
    void getPerson(Person[] persons, Person expected){
        //Given
        cut.setPersons(persons);
        //when
        Person actual = cut.getPerson(2);
        // Then
        Assertions.assertEquals(expected, actual);
    }
}
