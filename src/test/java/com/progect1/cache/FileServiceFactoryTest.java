package test.java.com.progect1.cache;

import main.java.com.progect1.cache.FileServiceFactory;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.services.fileservice.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;


import static org.junit.jupiter.api.Assertions.*;

class FileServiceFactoryTest {

    @BeforeAll
    static void init(){
        services[0] = Mockito.mock(JsonFileService.class);
        services[1] = Mockito.mock(XmlFileService.class);
        services[2] = Mockito.mock(YamlFileService.class);
        services[3] = Mockito.mock(BinaryFileService.class);
        services[4] = Mockito.mock(CsvFileService.class);
    }

    private static AbstractFileService[] services = new AbstractFileService[5];
    private final FileServiceFactory cut = new FileServiceFactory(services);

    static Arguments[] getFileServiceByIdTestArgs(){
        return new Arguments[]{
                Arguments.arguments(JsonFileService.class, 0),
                Arguments.arguments(XmlFileService.class, 1),
                Arguments.arguments(YamlFileService.class, 2),
                Arguments.arguments(BinaryFileService.class, 3),
                Arguments.arguments(CsvFileService.class, 4),
        };
    }

    @ParameterizedTest
    @MethodSource("getFileServiceByIdTestArgs")
    void getFileServiceByIdTest(Class expected, int id){

        AbstractFileService actual = cut.getFileServiceById(id);

        assertTrue(actual.getClass() == expected);
    }

    @Test
    void getFileServiceByIdTestWithException(){
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> cut.getFileServiceById(6));
    }
}