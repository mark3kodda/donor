package test.java.com.progect1.services;

import main.java.com.progect1.cache.FileServiceFactory;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.CommandService;
import main.java.com.progect1.services.PersonService;
import main.java.com.progect1.services.fileservice.AbstractFileService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class CommandServiceTest {

        private final Scanner scanner = Mockito.mock(Scanner.class);
        private final FileServiceFactory fileServiceFactory = Mockito.mock(FileServiceFactory.class);
        private final PersonService personService = Mockito.mock(PersonService.class);
        private final AbstractFileService fileService = Mockito.mock(AbstractFileService.class);
        private final PersonRepo personRepo = Mockito.mock(PersonRepo.class);

        private final CommandService cut = new CommandService(scanner, fileServiceFactory, personService);

        @Test
        void getCommandFromConsoleTest(){
                String expected = "create";
                Mockito.when(scanner.nextLine()).thenReturn(expected);
                String actual = cut.getCommandFromConsole();

                assertEquals(expected, actual);
        }

//        ******************************

        static Arguments[] setFileServiceTestArgs(){
                return new Arguments[]{
                        Arguments.arguments(0),
                        Arguments.arguments(1),
                        Arguments.arguments(2),
                        Arguments.arguments(3),
                        Arguments.arguments(4),
                };
        }

        @ParameterizedTest
        @MethodSource("setFileServiceTestArgs")
        void setFileServiceTest(int id){
                //для currentFileService = fileService
                Mockito.when(scanner.nextLine()).thenReturn(String.valueOf(id));
                Mockito.when(fileServiceFactory.getFileServiceById(id)).thenReturn(fileService);
                fileService.personRepo = personRepo;
                cut.setFileService();

                Mockito.verify(fileServiceFactory, Mockito.times(1)).getFileServiceById(id);
        }

        @Test
        void setFileServiceTestWithException(){
                Mockito.when(scanner.nextLine()).thenReturn(String.valueOf(5));
                assertThrows(StackOverflowError.class, cut::setFileService);
        }

        @Test
        void setFileServiceTestWithException2(){
                int firstTry = 5;
                int secondTry = 1;
                Mockito.when(scanner.nextLine()).thenReturn(String.valueOf(firstTry), String.valueOf(secondTry));
                Mockito.when(fileServiceFactory.getFileServiceById(secondTry)).thenReturn(fileService);
                fileService.personRepo = personRepo;

                cut.setFileService();

                Mockito.verify(fileServiceFactory, Mockito.times(1)).getFileServiceById(secondTry);
        }
//        ******************************

        static Arguments[] processCommandTestArgs(){
                return new Arguments[]{
                        Arguments.arguments("create", 1, 0, 0, 0, 0, 0, 0),
                        Arguments.arguments("readAll", 0, 1, 0, 0, 0, 0, 0),
                        Arguments.arguments("readById", 0, 0, 1, 0, 0, 0, 0),
                        Arguments.arguments("deleteById", 0, 0, 0, 1, 0, 0, 0),
                        Arguments.arguments("updateById", 0, 0, 0, 0, 1, 0, 0),
                        Arguments.arguments("start", 0, 0, 0, 0, 0, 1, 0),
                        Arguments.arguments("printAll", 0, 0, 0, 0, 0, 0, 1),
        };
}

        @ParameterizedTest
        @MethodSource("processCommandTestArgs")
        void processCommandTest(String command, int countCreate, int countReadAll,int countReadById, int countDeleteById,
                                int countUpdateById, int countStart, int countPrint){
                //для для того щоб зробити currentFileService = fileService
                int idOfFileService = 0;
                Mockito.when(scanner.nextLine()).thenReturn(String.valueOf(idOfFileService));
                Mockito.when(fileServiceFactory.getFileServiceById(idOfFileService)).thenReturn(fileService);
                fileService.personRepo = personRepo;
                cut.setFileService();

                //CMD_READ_ALL
                Person[] personsFromReadFile = new Person[0];
                Mockito.when(fileService.readFile()).thenReturn(personsFromReadFile);

                //CMD_READ_BY_ID  &&  CMD_DELETE_BY_ID  &&  CMD_UPDATE_BY_ID
                Person personFromGetPerson = new Person();
                int idOfPerson = 1;
                Mockito.when(scanner.nextLine()).thenReturn(String.valueOf(idOfPerson));
                Mockito.when(personRepo.getPerson(idOfPerson)).thenReturn(personFromGetPerson);

                cut.processCommand(command);

                Mockito.verify(personService, Mockito.times(countCreate)).create();//CMD_CREATE
                Mockito.verify(personRepo, Mockito.times(countReadAll)).setPersons(personsFromReadFile);//CMD_READ_ALL
                Mockito.verify(personService, Mockito.times(countReadById)).printPerson(personFromGetPerson);//CMD_READ_BY_ID
                Mockito.verify(personService, Mockito.times(countDeleteById)).delete(personFromGetPerson);//CMD_DELETE_BY_ID
                Mockito.verify(personService, Mockito.times(countUpdateById)).update(personFromGetPerson);//CMD_UPDATE_BY_ID
                //CMD_SWITCH визиває вже протестований setFileService()
                Mockito.verify(fileService, Mockito.times(countStart)).writeFile();//CMD_START
                //CMD_EXIT визиває вже протестований getCommandFromConsole()
                //CMD_HELP визиває тільки System.out.println()
                Mockito.verify(personService, Mockito.times(countPrint)).printAllPersons();//CMD_PRINT_ALL
        }

}