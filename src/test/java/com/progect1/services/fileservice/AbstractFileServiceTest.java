package test.java.com.progect1.services.fileservice;

import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.fileservice.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class AbstractFileServiceTest {
    private static final Person[] persons = new Person[2];
    private static final PersonRepo personRepo = new PersonRepo();
    private static final String fileName = "Test.test";

    private static final AbstractFileService json = new JsonFileService(fileName, personRepo);
    private static final AbstractFileService csv = new CsvFileService(fileName,personRepo);
    private static final AbstractFileService binary = new BinaryFileService(fileName,personRepo);
    private static final AbstractFileService xml = new XmlFileService(fileName, personRepo);
    private static final AbstractFileService yml = new YamlFileService(fileName, personRepo);


    @BeforeAll
    static void init(){
        persons[0] = new Person(123,"q","w",1,"e");
        persons[1] = new Person(543,"s","a",1,"d");
        personRepo.setPersons(persons);
    }

    static Arguments[] readWriteTestArgs(){
        return new Arguments[]{
                Arguments.arguments(json),
                Arguments.arguments(csv),
                Arguments.arguments(binary),
                Arguments.arguments(xml),
                Arguments.arguments(yml),
        };
    }

    @ParameterizedTest
    @MethodSource("readWriteTestArgs")
    void readWriteTest(AbstractFileService fileService){
        fileService.writeFile();

        Person[] actual = fileService.readFile();

        assertArrayEquals(persons, actual);

    }
}