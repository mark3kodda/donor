package test.java.com.progect1.services;

import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.PersonService;
import main.java.com.progect1.services.fileservice.AbstractFileService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class PersonServiceTest {

    private final AbstractFileService currentFileService = Mockito.mock(AbstractFileService.class);
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final PersonRepo personRepo = Mockito.mock(PersonRepo.class);
//    private final Person p = Mockito.mock(Person.class);

    public PersonService cut = new PersonService(scanner);

    static Arguments[] createTestArgs(){
//                Person.unicId = 0;
        return new Arguments[]{
                Arguments.arguments("Marko","Pollo","32", "Kharkov",1),
                Arguments.arguments("Marko","Pollo","121", "Kharkov",0),
                Arguments.arguments("Marko","Pollo","-1", "Kharkov",0),
                Arguments.arguments("","Pollo","32", "Kharkov",0),
                Arguments.arguments("Marko","","32", "Kharkov",0),
                Arguments.arguments("Marko","Pollo","32", "",0),
                Arguments.arguments("  ","Pollo","32", "Kharkov",0),
                Arguments.arguments(null,"Pollo","32", "Kharkov",0),
        };
    }

    @ParameterizedTest
    @MethodSource("createTestArgs")
    void createTest(String fName, String lName, String ageString, String city, int countAdd) {
        cut.setCurrentFileService(currentFileService);
        currentFileService.personRepo = personRepo;

        Mockito.when(scanner.nextLine()).thenReturn(fName, lName, ageString, city );

        cut.create();

        int age = Integer.parseInt(ageString);
        Person newPerson = new Person(1,fName,lName,age,city);

        Mockito.verify(personRepo,Mockito.times(countAdd)).add(newPerson);

    }
    @Test
    void createInvalidAgeTest() {
        cut.setCurrentFileService(currentFileService);
        currentFileService.personRepo = personRepo;

        String fName = "a";
        String lName = "d";
        String ageString = "two";
        String city = "x";

        Mockito.when(scanner.nextLine()).thenReturn(fName, lName, ageString, city );

        cut.create();

        Mockito.verify(personRepo,Mockito.times(0)).add(null);

    }

    @Test
    void deleteTest() {
        cut.setCurrentFileService(currentFileService);
        currentFileService.personRepo = personRepo;
        Person p = new Person();

        cut.delete(p);

        Mockito.verify(personRepo, Mockito.times(1)).remove(p);
    }

    @Test
    void deleteNullPersonTest() {
        cut.setCurrentFileService(currentFileService);
        currentFileService.personRepo = personRepo;
        Person p = null;

        cut.delete(p);

        Mockito.verify(personRepo, Mockito.times(1)).remove(p);
    }

    @Test
    void updateTest() { //валидный
        Person p = new Person(125,"a","d",22,"g");
        String fName = "b";
        String lName = "c";
        String ageString = "21";
        String city = "w";

        Mockito.when(scanner.nextLine()).thenReturn(fName, lName, ageString, city );
        cut.update(p);

        Person p1 = new Person(125,"b","c",21,"w");
        assertEquals(p1,p);
    }

    static Arguments[] updateInvalidCaseTestArgs(){
            return new Arguments []{
                Arguments.arguments("", "d", "22","g"),
                Arguments.arguments("a", "", "22","g"),
                Arguments.arguments("a", "d", "-1","g"),
                Arguments.arguments("a", "d", "22",""),
                Arguments.arguments("a", "d", "1501","g"),
                Arguments.arguments("a", "d", "asd",""),
                Arguments.arguments(null, "d", "22",""),
                Arguments.arguments("a", null, "22",""),
                Arguments.arguments("a", "d", null,"g"),
                Arguments.arguments("a", "d", "23",null),

            };
    }

    @ParameterizedTest
    @MethodSource("updateInvalidCaseTestArgs")
    void updateInvalidCaseTest(String fName, String lName, String ageString, String city) { // неваладный
        Person p = new Person(225,"a","d",22,"g");

        Mockito.when(scanner.nextLine()).thenReturn(fName, lName, ageString, city );
        cut.update(p);

        Person p1 = new Person(225,"a","d",22,"g");
        assertEquals(p1,p); //если метод упадёт то персоны остануться одинаковы
    }
}