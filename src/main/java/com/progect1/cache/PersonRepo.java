package main.java.com.progect1.cache;

import main.java.com.progect1.models.Person;

public class PersonRepo {

    private Person[] persons = new Person[0];
// поменял private на public (вместо рефлексии для тестов)
    public void add(Person p){
        Person[] buffer = null;
        if(persons == null){
            buffer = new Person[1];
        } else {
            buffer = new Person[persons.length + 1];
            for (int i = 0; i <persons.length ; i++) {
                buffer[i] = persons[i];
            }
        }
        buffer[buffer.length-1] = p;
        persons = buffer;
    }

    public int getMaxId(){
        if(persons != null && persons.length != 0){
            int i;
            int max = persons[0].id;
            for (i = 1; i < persons.length; i++)
                if (persons[i].id > max)
                    max = persons[i].id;
                return max;
        }
        return 0;
    }

//    public void update(Person p){
//        Person[] buffer = new Person[persons.length ];
//        int counter = 0;
//        for (int i = 0; i < persons.length ; i++) {
//            if(persons[i].id != p.id){
//                buffer[counter] = persons[i];
//                counter++;
//                }else{
//                buffer[counter] = p;
//                counter++;
//                }
//        }
//        persons = buffer;
//        }

    public void remove(Person p){
        if(p == null){
            return;
        }
        Person[] buffer = new Person[persons.length - 1];
        int counter = 0;

        for (int i = 0; i < persons.length ; i++) {
            if(persons[i].id != p.id){
                buffer[counter] = persons[i];
                counter++;
            }
        }

        persons = buffer;

    }

    public Person getPerson(int index){
        for (Person p: persons) {
            if(p.id == index){
                return  p;
            }
        }
        System.out.println("Person with this Id not exist");
        return null;
    }

    public void setPersons(Person[] addedPersons){
        persons = addedPersons;
    }

    public Person[] getPersons() {
        return persons;
    }
}
