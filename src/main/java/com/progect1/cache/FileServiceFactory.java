package main.java.com.progect1.cache;

import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.fileservice.*;

public class FileServiceFactory {

    private AbstractFileService[] fileServiceRepo;

    public FileServiceFactory(AbstractFileService[] services) {
        this.fileServiceRepo = services;
        Person.unicId = getMaxIdFromAllFiles()/* + 1*/;
    }

    public AbstractFileService getFileServiceById(int id){
        return fileServiceRepo[id];
    }

    private int getMaxIdFromAllFiles(){
        int maxId = 0;
        int maxIdInCurrentFile;
        PersonRepo currentRepo = new PersonRepo();
        for(int i = 0; i < 5; i++){
            currentRepo.setPersons(fileServiceRepo[i].readFile());
            if(currentRepo.getPersons() == null){
                continue;
            }
            maxIdInCurrentFile =  currentRepo.getMaxId();
            if(maxIdInCurrentFile > maxId){
                maxId = maxIdInCurrentFile;
            }
        }
        return maxId;
    }

}
