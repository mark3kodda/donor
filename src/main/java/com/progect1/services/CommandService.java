package main.java.com.progect1.services;

import main.java.com.progect1.cache.FileServiceFactory;
import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.fileservice.AbstractFileService;

import java.util.Scanner;

public class CommandService {
    private Scanner scanner;
    private PersonService personService;
    private AbstractFileService currentFileService;
    private FileServiceFactory fileServiceFactory;

    private static final String CMD_HELP = "help";
    private static final String CMD_EXIT = "exit";
    private static final String CMD_SWITCH = "switch";
    private static final String CMD_START = "start";
    private static final String CMD_CREATE = "create";
    private static final String CMD_READ_BY_ID = "readById";
    private static final String CMD_UPDATE_BY_ID = "updateById";
    private static final String CMD_DELETE_BY_ID = "deleteById";
    private static final String CMD_READ_ALL = "readAll";
    private static final String CMD_PRINT_ALL = "printAll";



    public CommandService(Scanner scanner, FileServiceFactory fileServiceFactory, PersonService personService) {
        this.fileServiceFactory = fileServiceFactory;
        this.personService = personService;
        this.scanner = scanner;

    }

    public void setFileService(){
        System.out.println("Pls select type of container. (0)JSON (1)XML (2)YML (3)Binary (4)CSV");
        try {
            int container = Integer.parseInt(scanner.nextLine());
            if( container >= 0 && container <= 4) {
                currentFileService = fileServiceFactory.getFileServiceById(container);
                personService.setCurrentFileService(currentFileService);
                readAll();
            }else {
                throw new NumberFormatException();
            }
        }catch (NumberFormatException e){
            System.out.println("Invalid command");
            setFileService();
        }
    }

    public void processCommand(String command){
        switch (command) {
            case CMD_CREATE:
                create();
                break;
            case CMD_READ_ALL:
                readAll();
                break;
            case CMD_READ_BY_ID:
                readById();
                break;
            case CMD_DELETE_BY_ID:
                deleteById();
                break;
            case CMD_UPDATE_BY_ID:
                updateById();
                break;
            case CMD_SWITCH:
                switchContainerType();
                break;
            case CMD_START:
                start();
                break;
            case CMD_EXIT:
                exit();
                break;
            case CMD_HELP:
                help();
                break;
            case CMD_PRINT_ALL:
                print();
                break;
            default:
                System.out.println("Invalid command! plz try again");
                break;
        }
    }

    public String getCommandFromConsole() {
        String nextCommand = scanner.nextLine();
         return nextCommand;
    }

    private void create(){
        personService.create();
    }

    private Person getPersonById(){
        System.out.println("Pls add Id of Person");
        int id=0;
        try {
            id = Integer.parseInt(scanner.nextLine());
            if(id<0) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("Not correct Id");
            return null;
        }
        return currentFileService.personRepo.getPerson(id);
    }

    private void readAll(){
        currentFileService.personRepo.setPersons(currentFileService.readFile());
    }

    private void readById(){
        Person p = getPersonById();
        if(p == null){
            return;
        }
        personService.printPerson(p);
    }

    private void deleteById(){
        Person p = getPersonById();
        if(p == null){
            return;
        }
        personService.delete(p);
    }

    private void updateById(){
        Person p = getPersonById();
        if(p == null){
            return;
        }
        personService.update(p);
    }

    private void switchContainerType(){
        setFileService();
    }

    private void start(){
        currentFileService.writeFile();
    }

    private void print(){
        personService.printAllPersons();
    }


    private void exit(){
        System.out.println("You want exit?(Yes/No)");
        if(getCommandFromConsole().equalsIgnoreCase("yes")){
            System.out.println("Goodbye!");
            System.exit(0);
        }
    }


    private void help(){
        System.out.println("LIST OF COMMANDS\n" +
                "create - to create new person\n" +
                "readById - to read persons by id\n" +
                "readAll - to read all persons at the selected container\n" +
                "updateById - to update person by Id\n" +
                "deleteById - to delete person by Id\n" +
                "printAll - to print all persons to console\n" +
                "switch - to choose container\n" +
                "start - to save data to container\n" +
                "help - to display available commands\n" +
                "exit - to to exit the application\n" +
                "***********************************\n");
    }

}
