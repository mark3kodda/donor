package main.java.com.progect1.services;

import main.java.com.progect1.models.Person;
import main.java.com.progect1.services.fileservice.AbstractFileService;
import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

public class PersonService {
    private AbstractFileService currentFileService;
    private Scanner scanner;

    public PersonService(Scanner scanner) {
        this.scanner = scanner;
    }

    public void setCurrentFileService(AbstractFileService currentFileService) {
        this.currentFileService = currentFileService;
    }

    public void printAllPersons(){
        if(currentFileService.personRepo.getPersons() != null && currentFileService.personRepo.getPersons().length != 0) {
            for (int i = 0; i < currentFileService.personRepo.getPersons().length; i++) {
                System.out.println(currentFileService.personRepo.getPersons()[i].toString());
            }
            System.out.println();
        }else{
            System.out.println("[]");
        }
    }

    public void printPerson(Person p){
        System.out.println(p.toString());
    }

    private void validate(String field){
        if (StringUtils.isBlank(field) || field.length() > 256){
            throw new IllegalArgumentException();
        }
    }
    private void validate(int field){
        if(field < 0 || field > 120){
            throw new NumberFormatException();
        }
    }

    public void create() {
        System.out.println("Enter new Person info");
        String setFirstName;
        String setLastName;
        String setCity;
        int setedAge;
        try {
            System.out.println("Enter first name");
            setFirstName = scanner.nextLine();
            validate(setFirstName);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid first name");
            return;
        }
        try {
            System.out.println("Enter last name");
            setLastName = scanner.nextLine();
            validate(setLastName);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid last name");
            return;
        }

        try {
            System.out.println("Enter age");
            setedAge = Integer.parseInt(scanner.nextLine());
            validate(setedAge);

        } catch (NumberFormatException e) {
            System.out.println("Invalid age");
            return;

        }
        try {
            System.out.println("Enter city");
            setCity = scanner.nextLine();
            validate(setCity);
        } catch (IllegalArgumentException e){
            System.out.println("Invalid city");
            return;
        }
        Person createdPerson = new Person(setFirstName, setLastName, setedAge, setCity);
        currentFileService.personRepo.add(createdPerson);
    }

    public void delete(Person p){
        currentFileService.personRepo.remove(p);
    }

    public void update(Person p){
        System.out.println("Set new Person info");
        String setFirstName;
        String setLastName;
        String setCity;
        int setedAge;
        try {
            System.out.println("Enter first name");
            setFirstName = scanner.nextLine();
            validate(setFirstName);
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("Invalid first name");
            return;
        }

        try {
            System.out.println("Enter last name");
            setLastName = scanner.nextLine();validate(setLastName);
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("Invalid last name");
            return;
        }

        try {
            System.out.println("Enter age");
            setedAge = Integer.parseInt(scanner.nextLine());validate(setedAge);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Invalid age");
            return;
        }
        try {
            System.out.println("Enter city");
                        setCity = scanner.nextLine();validate(setCity);
        } catch (IllegalArgumentException e){
            System.out.println("Invalid city name");
            return;
        }
        p.fname = setFirstName;
        p.lname = setLastName;
        p.age = setedAge;
        p.city = setCity;
//        currentFileService.personRepo.update(p);
        }
    }
