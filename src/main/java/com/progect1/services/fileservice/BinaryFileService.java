package main.java.com.progect1.services.fileservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.undercouch.bson4jackson.BsonFactory;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;

import java.io.IOException;

public class BinaryFileService extends AbstractFileService {

    //это объект Jackson, который выполняет сериализацию
    private ObjectMapper mapper=new ObjectMapper(new BsonFactory());

    public BinaryFileService(String fileName, PersonRepo personRepo) {
        super(fileName, personRepo);
    }


    @Override
    public Person[] readFile() {
        if(file.length() == 0){
            return null;
        }
        Person[] readedList =  null;
        try {
            //десериализация
            readedList = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            System.out.println("Problem with read in binary ");
            e.printStackTrace();
        }
        return readedList;
    }

    @Override
    public void writeFile() {
        try {
            mapper.writeValue(file, personRepo.getPersons());
        } catch (IOException e) {
            System.out.println("Problem with write in binary "+e.getMessage());
            e.printStackTrace();
        }
    }
}
