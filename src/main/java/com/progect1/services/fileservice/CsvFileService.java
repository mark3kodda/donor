package main.java.com.progect1.services.fileservice;



import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvFileService extends AbstractFileService{

    public CsvFileService(String fileName, PersonRepo personRepo) {
        super(fileName, personRepo);
    }

    @Override
    public Person[] readFile() {
        if(file.length() == 0){
            return null;
        }
        List<Person> result = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean<Person> csvToBean = new CsvToBeanBuilder<Person>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            result = csvToBean.parse();
        }catch(IOException e){
            System.out.println("Problem with read in csv ");
            e.printStackTrace();
        }

        return result.toArray(new Person[0]);
    }

    @Override
    public void writeFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder<Person>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(Arrays.asList(personRepo.getPersons()));
        }catch(CsvDataTypeMismatchException | CsvRequiredFieldEmptyException |
                IOException e){
            System.out.println("Problem with write in csv ");
            e.printStackTrace();
        }
    }
}
