package main.java.com.progect1.services.fileservice;

import main.java.com.progect1.cache.PersonRepo;
import java.io.File;
import java.io.IOException;

public abstract class AbstractFileService implements FileService{
    private static final String PATCH_TO_FILES = "src/main/java/com/progect1/files/";
    protected File file;
    public PersonRepo personRepo;

    public AbstractFileService(String fileName, PersonRepo personRepo) {
        this.file = createFile(fileName);
        this.personRepo = personRepo;
    }

    private File createFile(String name){
        File file = new File(PATCH_TO_FILES + name);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Problem with creating file  ");
                e.printStackTrace();
            }
        }
        return file;
    }
}
