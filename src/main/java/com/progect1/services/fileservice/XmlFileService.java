package main.java.com.progect1.services.fileservice;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;

import java.io.IOException;

public class XmlFileService extends AbstractFileService {
    //это объект Jackson, который выполняет сериализацию
   private XmlMapper mapper=new XmlMapper();

    public XmlFileService(String fileName, PersonRepo personRepo) {
        super(fileName, personRepo);
    }


    @Override
    public Person[] readFile() {
        if(file.length() == 0){
            return null;
        }
        Person[] readedList =  null;
        try {
            //десериализация
            readedList = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            System.out.println("Problem with read in xml ");
            e.printStackTrace();
        }
        return readedList;
    }

    @Override
    public void writeFile() {
        try {
            mapper.writeValue(file, personRepo.getPersons());
        } catch (IOException e) {
            System.out.println("Problem with write in xml ");
            e.printStackTrace();
        }
    }
}
