package main.java.com.progect1.services.fileservice;

import main.java.com.progect1.models.Person;

public interface FileService {

    Person[] readFile();

    void writeFile();

}
