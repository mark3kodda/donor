package main.java.com.progect1.services.fileservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.models.Person;

import java.io.*;

public class YamlFileService extends AbstractFileService {

    //это объект Jackson, который выполняет сериализацию
    private ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public YamlFileService(String fileName, PersonRepo personRepo) {
        super(fileName, personRepo);
    }


    @Override
    public Person[] readFile() {
        if(file.length() == 0){
            return null;
        }
        Person[] readedList =  null;
        try {
            //десериализация
            readedList = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            System.out.println("Problem with read in yml ");
            e.printStackTrace();
        }
        return readedList;
    }

    @Override
    public void writeFile() {
        try {
            mapper.writeValue(file, personRepo.getPersons());
        } catch (IOException e) {
            System.out.println("Problem with write in yml ");
            e.printStackTrace();
        }
    }
}