package main.java.com.progect1;

import main.java.com.progect1.services.CommandService;

public class Project {
    public CommandService commandService;

    public Project(CommandService commandService) {
        this.commandService = commandService;
    }

    public void run(){
        System.out.println("Welcome!");
        commandService.setFileService();


        String nextCommand;

        while(true){
            System.out.println("Write command (\"help\" - if you need list of command) ");
            nextCommand = commandService.getCommandFromConsole();
            commandService.processCommand(nextCommand);
        }
    }
}
