package main.java.com.progect1;

import main.java.com.progect1.cache.FileServiceFactory;
import main.java.com.progect1.cache.PersonRepo;
import main.java.com.progect1.services.CommandService;
import main.java.com.progect1.services.PersonService;
import main.java.com.progect1.services.fileservice.*;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        AbstractFileService[] services = new AbstractFileService[5];
        services[0] = new JsonFileService("JsonFile.json", new PersonRepo());
        services[1] = new XmlFileService("XmlFile.xml", new PersonRepo());
        services[2] = new YamlFileService("YmlFile.yml", new PersonRepo());
        services[3] = new BinaryFileService("BinaryFile.binary", new PersonRepo());
        services[4] = new CsvFileService("CsvFile.csv", new PersonRepo());

        Scanner scanner = new Scanner(System.in);
        FileServiceFactory fileServiceFactory = new FileServiceFactory(services);
        PersonService personService = new PersonService(scanner);
        CommandService commandService = new CommandService(scanner, fileServiceFactory, personService);
        Project project = new Project(commandService);

        project.run();
    }
}
