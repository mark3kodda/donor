package main.java.com.progect1.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.opencsv.bean.CsvBindByName;

import java.util.Objects;

@JsonAutoDetect
public class Person{
    public static int unicId = 0;
    @CsvBindByName
    public int id;
    @CsvBindByName
    public String fname;
    @CsvBindByName
    public String lname;
    @CsvBindByName
    public int age;
    @CsvBindByName
    public String city;

    public Person(String fname, String lname, int age, String city) {
        this.id = ++unicId;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person(int id, String fname, String lname, int age, String city) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person(){
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id && age == person.age && Objects.equals(fname, person.fname) && Objects.equals(lname, person.lname) && Objects.equals(city, person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fname, lname, age, city);
    }
}
