![](dbs.png)
## Симулятор базы данных
____
***Симулятор базы данных*** - позволяет обрабатывать список людей в файлах, различных форматов. Создан в рамках обучения [DevEducation](https://deveducation.com/en/) - Base Java 2021. Ukraine.

#### Установка
Загрузить дерево проекта. Открыть его в среде разработки.

#### Предпосылки
Все необходимые библиотеки были включены в раздачу.

#### Список команд
- create - создание новой персоны
- readById - считываем персону по id
- readAll - считываем всех персон в выбраном контейнере
- updateById - обновление данных персоны по id
- deleteById - удаление данный по id
- printAll - вывод в консоль всех персон
- switch - выбор контейнера для текущей работы
- start - внесение изменений в текущий контейнер
- help - отображение списка команд
- exit - выход из приложения

#### Создатели
[@Serhii9Sol](https://bitbucket.org/Serhii9Sol/)
[@alonadrobot](https://bitbucket.org/alonadrobot/)
[@maximbiba](https://bitbucket.org/maximbiba/)
[@mark3kodda](https://bitbucket.org/mark3kodda/)